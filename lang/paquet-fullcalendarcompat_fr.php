<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'fullcalendarcompat_description' => 'Permet de passer à SPIP 3.3 sans avoir à adapter ses squelettes pour la nouvelle version de FullCalendar',
	'fullcalendarcompat_nom' => 'FullCalendar Compat',
	'fullcalendarcompat_slogan' => 'FullCalendar de SPIP 3.2 pour SPIP 3.3+',
);
